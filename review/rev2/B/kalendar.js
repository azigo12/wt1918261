let pomocnaMjesec = new Date().getMonth();

var periodicnaSala1 = {dan:5, semestar:"zimski", pocetak:"09:00", kraj:"10:00", naziv:"0-09", predavac:"profesor"};//neispravan podatak
var periodicnaSala2 = {dan:0, semestar:"zimski", pocetak:"10:00", kraj:"13:00", naziv:"0-09", predavac:"profesor"};

var periodicnaSala3 = {dan:5, semestar:"ljetni", pocetak:"12:00", kraj:"13:00", naziv:"1-02", predavac:"profesor"};
var periodicnaSala4 = {dan:3, semestar:"ljetni", pocetak:"12:00", kraj:"13:00", naziv:"VA1", predavac:"profesor"};

let listaPeriodicnihSala = [periodicnaSala1, periodicnaSala2, periodicnaSala3, periodicnaSala4];

let vanrednaSala1 = {datum:"10.10.2019", pocetak:"10:10", kraj:"12:00", naziv:"0-09", predavac:"profesor"};
let vanrednaSala2 = {datum:"31.11.2019", pocetak:"13:00", kraj:"17:00", naziv:"0-09", predavac:"profesor"};//neispravan podatak
let vanrednaSala3 = {datum:"17.11.2019", pocetak:"13:00", kraj:"17:00", naziv:"0-09", predavac:"profesor"};
let vanrednaSala4 = {datum:"09.09.2019", pocetak:"13:00", kraj:"17:00", naziv:"VA2", predavac:"profesor"};
let vanrednaSala5 = {datum:"09.11.2019", pocetak:"13:00", kraj:"17:00", naziv:"0-01", predavac:"profesor"};

let listaVanrednihSala = [vanrednaSala1,vanrednaSala2, vanrednaSala3, vanrednaSala4, vanrednaSala5];  

let ljetniSemestar = ["Februar", "Mart", "April", "Maj", "Juni"];
let zimskiSemestar = ["Januar", "Oktobar", "Novembar", "Decembar"];
let listaMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
    
let mapaPocetnihDana = new Map([[0,  1], [1,  4], [2,  4], [3,  0], [4,  2], [5,  5], [6,  0], [7, 3], [8,  6],[9,  1], [10,  4], [11,  6]]);
let mapaMjeseciSaBrojemDana = new Map([[0,  31], [1,  28], [2,  31], [3,  30], [4,  31], [5,  30], [6,  31], [7, 31], [8,  30],[9,  31], [10,  30], [11,  31]]);

let unesenPocetak = 0;
let unesenKraj = 0;

let Kalendar = (function(){

function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){

let pocetniDan = mapaPocetnihDana.get(mjesec);
let salaIzRezervacije = sala;
if (pocetak != 0 && kraj != 0)
{
        for (let i = 0; i < listaPeriodicnihSala.length; i++)
        {   
            if (validirajVrijednostiPeriodicna(listaPeriodicnihSala[i].dan, listaPeriodicnihSala[i].pocetak, listaPeriodicnihSala[i].kraj) == 1){
            let zauzeta = unutarVremena(pocetak, kraj, listaPeriodicnihSala[i].pocetak, listaPeriodicnihSala[i].kraj);
            if (zauzeta == 1)
            {
                let prviDanZaBojiti = izracunajPocetniDan(pocetniDan, listaPeriodicnihSala[i].dan);
                if (listaPeriodicnihSala[i].semestar == "zimski"){
                    if (listaPeriodicnihSala[i].naziv == salaIzRezervacije){
                    for (let m = 0; m < zimskiSemestar.length; m++) 
                    {
                        if (listaMjeseci[mjesec] == zimskiSemestar[m])
                        {
                            let celija = document.getElementsByClassName("bojaCelije");
                            for (let v = prviDanZaBojiti; v < celija.length; v+=7)
                            {
                                celija[v].children[0].className = "zauzeta";
                            }
                        
                        }
                    }
                }
            }
                else if (listaPeriodicnihSala[i].semestar == "ljetni")
                {
                    for (let m = 0; m < ljetniSemestar.length; m++) 
                    {
                        if (listaMjeseci[mjesec] == ljetniSemestar[m])
                        {
                            let celija = document.getElementsByClassName("bojaCelije");
                            for (let v = prviDanZaBojiti; v < celija.length; v+=7)
                            {
                                celija[v].children[0].className = "zauzeta";
                            } 
                        }
                    }  
                }
            }
        }
    }

    for (let i = 0; i < listaVanrednihSala.length; i++)
    {
        if (validirajVrijednostiNeperiodicna(listaVanrednihSala[i].datum, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj) == 1)
        {
            let zauzeta = unutarVremena(pocetak, kraj, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj);
            if (zauzeta == 1)
            {
                if (listaVanrednihSala[i].naziv == salaIzRezervacije)
                {
                    let datumSale = listaVanrednihSala[i].datum;
                    let pom1 = datumSale.split(".");
                    let danSale = pom1[0];
                    danSale--;
                    let mjesecSale = pom1[1];
                    mjesecSale--;

                    if (pomocnaMjesec == mjesecSale)
                    {
                        let celija = document.getElementsByClassName("bojaCelije");
                        celija[danSale].children[0].className = "zauzeta";
                    }
                }
            }
        }
    }
}
}
function ucitajPodatkeImpl(periodicna, redovna)
{   
    listaPeriodicnihSala = periodicna.slice();
    listaVanrednihSala = redovna.slice();
}

function iscrtajKalendarImpl(kalendarRef, mjesec)
{
    let today = new Date();
    let listaDana = ["PON", "UTO", "SRI", "CET", "PET", "SUB", "NED"];
    let trenutnaGodina = today.getFullYear();
    let firstDay = (new Date(trenutnaGodina + "-" + mjesec + "-01")).getDay()+2;
    let daysInMonth = 32 - new Date(trenutnaGodina, mjesec, 32).getDate();
    let date = 1;
    let tabela1 = document.createElement("table");
    tabela1.className="tabela1";
    if ( mjesec == 4 || mjesec == 6 || mjesec == 9 || mjesec == 11) firstDay--;
    if (mjesec == 2) firstDay -= 3;
    if (mjesec == 0) firstDay = 1;
    for (let i = 1; i < 9; i++){
        var red = document.createElement("tr");
        for (let j = 0; j < 7; j++){
             if (i == 1){
                let mjesec2 = document.createElement("td");
                mjesec2.className="mjesec";
                if (j == 0){
                    let tekstMjesec = document.createTextNode(listaMjeseci[mjesec]);
                    mjesec2.appendChild(tekstMjesec);
                }
                red.appendChild(mjesec2);
            }

            else if (i == 2){
                let dani = document.createElement("td");
                dani.className = "dani";
                let tekstDani = document.createTextNode(listaDana[j]);
                dani.appendChild(tekstDani);
                red.appendChild(dani);
            }

            else if (i == 3 && j < firstDay) {
                let cell = document.createElement("td");
                let cellText = document.createTextNode("");
                cell.appendChild(cellText);
                red.appendChild(cell);
                tabela1.appendChild(red);
            }
           
            else {
                let celija = document.createElement("td");
                let tabela2 = document.createElement("table");
                tabela2.className="tabela2";
                celija.appendChild(tabela2);
                for  (let m = 0; m < 2; m++){
                    var redU = document.createElement("tr");
                    
                        if (m==0){
                            let kolonaU = document.createElement("td");
                            let danUKal = document.createTextNode(date);
                            kolonaU.appendChild(danUKal);
                            redU.className = "brojDana";
                            redU.appendChild(kolonaU);
                            date++;
                        }
                        else if (m==1){
                            let kolonaU2 = document.createElement("td");
                            kolonaU2.className = "slobodna";
                            redU.className = "bojaCelije";
                            redU.appendChild(kolonaU2);
                        }
                        tabela2.appendChild(redU);  
                }
                if (date-1 > daysInMonth) {
                    break;
                }
                celija.appendChild(tabela2);
                red.appendChild(celija);
            }
        }
       tabela1.appendChild(red);
    }
    kalendarRef.appendChild(tabela1);

}
return {
obojiZauzeca: obojiZauzecaImpl,
ucitajPodatke: ucitajPodatkeImpl,
iscrtajKalendar: iscrtajKalendarImpl
}
}());

function next() {
    pomocnaMjesec++;
    if (pomocnaMjesec < 12){
        
        document.getElementsByClassName("dugmeS").disabled = false;
        let today = new Date();
        let pozicija = today.getMonth();
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        let trenutniMjesec= new Date().getMonth();
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);

    }
    else {
        pomocnaMjesec = 11;
        document.getElementsByClassName("dugmeS").disabled = true;
    }
}

function previous() {
    pomocnaMjesec--;
    if (pomocnaMjesec >= 0){
        document.getElementsByClassName("dugmeP").disabled = false;
        let today = new Date();
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        let trenutniMjesec= new Date().getMonth();
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
    }
    else {
        pomocnaMjesec = 0;
        document.getElementsByClassName("dugmeP").disabled = true;
    }
}

function ucitajKalendar(){
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
}

function izracunajPocetniDan(pocetniDan, danListe){
    let prviDanZaBojiti = danListe;
            if (pocetniDan == 6 && danListe != 6) 
            {
                prviDanZaBojiti = prviDanZaBojiti + 1;
            }
            else if (pocetniDan > danListe)
            {
                prviDanZaBojiti = danListe + pocetniDan - 1;
            }
            else if (danListe > pocetniDan) 
            {
                prviDanZaBojiti = danListe - pocetniDan;
            }
            else if (danListe == pocetniDan)
            {
                prviDanZaBojiti = 0;
                if (pocetniDan - 7 > 0) pocetniDan -= 7; 
            }
            if ((danListe == 0 && (pomocnaMjesec == 9 || pomocnaMjesec == 0))) 
            {
                prviDanZaBojiti--;
                prviDanZaBojiti += 7;
            }
            if (pocetniDan == 0)
            {
                prviDanZaBojiti = danListe;
            }
            if (pomocnaMjesec == 5) prviDanZaBojiti -= 2;
            if (pomocnaMjesec == 5 && (danListe == 5 || danListe == 6)) prviDanZaBojiti += 2;
            if (pomocnaMjesec == 4 && (danListe == 0 || danListe == 1)) prviDanZaBojiti += 4;
            return prviDanZaBojiti;
}

function unutarVremena(pocetak, kraj, salaPocetak, salaKraj)
{
    let zauzeta = 0;
    if (pocetak > salaPocetak && pocetak < salaKraj) 
         zauzeta = 1;
    else if (pocetak < salaPocetak && kraj > salaPocetak)
        zauzeta = 1;
    else if (pocetak == salaPocetak || kraj == salaKraj)
        zauzeta = 1;    
    return zauzeta;    
}

function unesenoVrijemeZaPocetak()
{
    unesenPocetak = 1;
}

function unesenoVrijemeZaKraj()
{
    unesenKraj = 1;
}

function osvjeziKalendar()
{
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
}

function validirajVrijednostiPeriodicna(danZaValidaciju, pocetakZaValidaciju, krajZaValidaciju)
{
    if (danZaValidaciju > 6 || danZaValidaciju < 0) return 0;
    
    var satiPocetak = pocetakZaValidaciju.split(":")[0];
    var minutePocetak = pocetakZaValidaciju.split(":")[1];

    var satiKraj = krajZaValidaciju.split(":")[0];
    var minuteKraj = krajZaValidaciju.split(":")[1];

    if (satiPocetak > satiKraj) return 0;
    if (satiPocetak > 24 || satiPocetak < 0) return 0;
    if (satiKraj > 24 || satiKraj < 0) return 0;

    return 1;
}

function validirajVrijednostiNeperiodicna(datumZaValidaciju, pocetakZaValidaciju, krajZaValidaciju)
{
    let pom1 = datumZaValidaciju.split(".");
    let danSale = pom1[0];
    let mjesecSale = pom1[1];
    mjesecSale--;
    mjesecSale = parseInt(mjesecSale);
    let brojDanaMjeseca = mapaMjeseciSaBrojemDana.get(mjesecSale);
    
    if (mjesecSale > 12 || mjesecSale < 0) return 0;
    if (danSale > brojDanaMjeseca) return 0;

    var satiPocetak = pocetakZaValidaciju.split(":")[0];
    var minutePocetak = pocetakZaValidaciju.split(":")[1];

    var satiKraj = krajZaValidaciju.split(":")[0];
    var minuteKraj = krajZaValidaciju.split(":")[1];

    if (satiPocetak > satiKraj) return 0;
    if (satiPocetak > 24 || satiPocetak < 0) return 0;
    if (satiKraj > 24 || satiKraj < 0) return 0;

    return 1;
}