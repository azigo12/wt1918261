let mjesec = new Date().getMonth();
let pocetnoVrijeme = "00:00", krajnjeVrijeme = "23:59";
let sala = "0-01";

//Testni podaci
const testRedovnih = [
    {
        dan: 0,
        semestar: "zimski",
        pocetak: "12:00",
        kraj: "13:00",
        naziv: "0-01",
        predavac: "V. Ljubovic"
    },
    {
        dan: 2,
        semestar: "zimski",
        pocetak: "10:00",
        kraj: "14:00",
        naziv: "0-02",
        predavac: "V. Ljubovic"
    },
    {
        dan: 3,
        semestar: "zimski",
        pocetak: "09:00",
        kraj: "11:00",
        naziv: "0-01",
        predavac: "V. Ljubovic"
    },

    {
        dan: 4,
        semestar: "ljetni",
        pocetak: "08:00",
        kraj: "14:00",
        naziv: "0-02",
        predavac: "V. Ljubovic"
    },
    {
        dan: 2,
        semestar: "ljetni",
        pocetak: "12:00",
        kraj: "14:00",
        naziv: "0-01",
        predavac: "V. Ljubovic"
    }
];

const testVanrednih = [
    {
        datum: "13.05.2019",
        pocetak: "09:00",
        kraj: "11:00",
        naziv: "0-02",
        predavac: "B. Mesihovic"
    },
    {
        datum: "18.05.2019",
        pocetak: "11:00",
        kraj: "13:00",
        naziv: "0-02",
        predavac: "A. Dautovic"
    },

    {
        datum: "12.05.2019",
        pocetak: "11:00",
        kraj: "14:00",
        naziv: "0-01",
        predavac: "H. Fatkic"
    },

    {
        datum: "22.11.2019",
        pocetak: "11:00",
        kraj: "13:00",
        naziv: "0-01",
        predavac: "A. Dautovic"
    },

    {
        datum: "29.11.2019",
        pocetak: "11:00",
        kraj: "14:00",
        naziv: "0-01",
        predavac: "H. Fatkic"
    }

];

let Kalendar = (function () {

    let nizRedovnih = [];
    let nizVanrednih = [];

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        const zimskiMjeseci = [0, 9, 10, 11];
        const ljetniMjeseci = [1, 2, 3, 4, 5];

        nizRedovnih.forEach(redovno => {
            if (((redovno.semestar === "zimski" && zimskiMjeseci.indexOf(mjesec) !== -1) ||
                (redovno.semestar === "ljetni" && ljetniMjeseci.indexOf(mjesec) !== -1)) &&
                redovno.naziv === sala &&
                redovno.pocetak >= pocetak &&
                redovno.kraj <= kraj) {
                let dani = kalendarRef.children[1].getElementsByTagName('td');
                let pocetak = redovno.dan;
                for (let i = 0; i < dani.length; i++) {
                    if (i === pocetak) {
                        // Boji, dodavanje css klase
                        dani[i].classList.add("zauzeto");
                        pocetak += 7;
                    }
                }
            }
        });

        nizVanrednih.forEach(vanredno => {
            let datum = vanredno.datum.split('.'); // datum[0] = dan, datum[1] = mjesec
            if (vanredno.naziv === sala && vanredno.pocetak >= pocetak && vanredno.kraj <= kraj && mjesec == datum[1] - 1) {
                let dani = kalendarRef.children[1].getElementsByTagName('td');
                for (let i = 0; i < dani.length; i++)
                    if (dani[i].innerText == datum[0])
                        dani[i].classList.add("zauzetoVanredno");
            }
        });
    }

    function ucitajPodatkeImpl(periodicna, vanredna) {
        nizRedovnih = Array.from(periodicna);
        nizVanrednih = Array.from(vanredna);
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        const imena_mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
        // Ime mjeseca, kao neki heding
        let ime_mjeseca = document.createElement('h3');
        ime_mjeseca.innerText = imena_mjeseci[mjesec];
        kalendarRef.appendChild(ime_mjeseca);

        // Imena dana 
        let tabela = document.createElement('table');
        kalendarRef.appendChild(tabela);
        const imena_dana = ["PON", "UTO", "SRI", "CET", "PET", "SUB", "NED"];
        let tr = document.createElement('tr');
        for (let i = 0; i < 7; i++) {
            let th = document.createElement('th');
            th.innerText = imena_dana[i];
            tr.appendChild(th);
        }
        tabela.appendChild(tr);
        // Dani

        // Prvi dan u izabranom mjesecu trenutne godine
        let datumPrvogDana = new Date(new Date().getFullYear(), mjesec, 1).toDateString();
        const imenaDanaEng = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        const indexPrvogDana = imenaDanaEng.indexOf(datumPrvogDana.substr(0, 3));
        const brojDana = new Date(new Date().getFullYear(), mjesec + 1, 0).getDate();
        tr = null;
        for (let i = 0; i < brojDana + indexPrvogDana; i++) {
            if (i % 7 === 0) {
                tr = document.createElement('tr');
            }
            let tekst = (i >= indexPrvogDana) ? i - indexPrvogDana + 1 : "";
            let td = document.createElement('td');
            td.innerText = tekst;
            if (i < indexPrvogDana) {
                td.classList.add("izbaci");
            }
            tr.appendChild(td);
            if (i % 7 === 6) {
                tabela.appendChild(tr);
            }
        }
        tabela.appendChild(tr);
    }

    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    }

}());

function dajSljedeciMjesec() {
    document.getElementById("lijevi").disabled = false;
    if (mjesec === 11) {
        document.getElementById("desni").disabled = true;
    } else {
        mjesec++;
        refresh();
    }
}

function dajPrethodniMjesec() {
    document.getElementById("desni").disabled = false;
    if (mjesec === 0) {
        document.getElementById("lijevi").disabled = true;
    } else {
        mjesec--;
        refresh();
    }
}

function promijeniPocetak() {
    pocetnoVrijeme = document.getElementById("pocetak").value;
    refresh();
}

function promijeniKraj() {
    krajnjeVrijeme = document.getElementById("kraj").value;
    refresh();
}

function promijeniSalu() {
    sala = document.getElementById("sala").value;
    refresh();
}

function refresh() {
    document.getElementById("kalendar").innerHTML = "";
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), mjesec);
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec, sala, pocetnoVrijeme, krajnjeVrijeme);
}

// Listeneri
document.getElementById("desni").addEventListener("click", dajSljedeciMjesec);
document.getElementById("lijevi").addEventListener("click", dajPrethodniMjesec);
document.getElementById("pocetak").addEventListener("change", promijeniPocetak);
document.getElementById("kraj").addEventListener("change", promijeniKraj);
document.getElementById("sala").addEventListener("change", promijeniSalu);

function main() {
    Kalendar.ucitajPodatke(testRedovnih, testVanrednih);
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), mjesec);
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec, sala, pocetnoVrijeme, krajnjeVrijeme);
}
