let Kalendar = (function() {
    //
    //ovdje idu privatni atributi
    //

    let trenutniMjesec = 0;
    let periodicnaGlobal = [];
    let vanrednaGlobal = [];

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        //implementacija ide ovdje

        if (mjesec === 13) iscrtajKalendarImpl(kalendarRef, trenutniMjesec);

        let vrijemePocetka = pocetak.split(":");
        let vrijemeKraja = kraj.split(":");
        for (let i = 0; i < vrijemePocetka.length; i++)
            vrijemePocetka[i] = Number(vrijemePocetka[i]);

        for (let i = 0; i < vrijemeKraja.length; i++)
            vrijemeKraja[i] = Number(vrijemeKraja[i]);

        let velicina = periodicnaGlobal.length;
        let temp = "";
        if (mjesec >= 9 || mjesec === 0) {
            temp = "zimski";
        } else if (mjesec >= 1 && mjesec <= 5) {
            temp = "ljetni";
        }

        let pon = kalendarRef.getElementsByClassName("0");
        let uto = kalendarRef.getElementsByClassName("1");
        let sri = kalendarRef.getElementsByClassName("2");
        let cet = kalendarRef.getElementsByClassName("3");
        let pet = kalendarRef.getElementsByClassName("4");
        let sub = kalendarRef.getElementsByClassName("5");
        let ned = kalendarRef.getElementsByClassName("6");

        let pocetakTrazeni = document
            .getElementById("pocetak")
            .value.split(":");
        let krajTrazeni = document.getElementById("kraj").value.split(":");

        //Implementacija bojenja periodicnih rezervacija
        for (let i = 0; i < velicina; i++) {
            let pocetakSala = periodicnaGlobal[i].pocetak.split(":");
            let krajSala = periodicnaGlobal[i].kraj.split(":");

            for (let j = 0; j < pocetakTrazeni.length; j++)
                pocetakTrazeni[j] = Number(pocetakTrazeni[j]);

            for (let j = 0; j < krajTrazeni.length; j++)
                krajTrazeni[j] = Number(krajTrazeni[j]);

            for (let j = 0; j < pocetakSala.length; j++)
                pocetakSala[j] = Number(pocetakSala[j]);

            for (let j = 0; j < krajSala.length; j++)
                krajSala[j] = Number(krajSala[j]);

            let uslov = true;

            if (
                pocetakTrazeni[0] >= krajSala[0] ||
                krajTrazeni[0] <= pocetakSala[0]
            ) {
                if (
                    krajTrazeni[0] === pocetakSala[0] &&
                    krajTrazeni[1] > pocetakSala[1]
                )
                    uslov = true;
                else if (
                    pocetakTrazeni[0] === krajSala[0] &&
                    pocetakTrazeni[1] < krajSala[1]
                ) {
                    uslov = true;
                } else uslov = false;
            }

            if (pocetakTrazeni[0] >= krajTrazeni[0]) {
                if (
                    pocetakTrazeni[0] === krajTrazeni[0] &&
                    pocetakTrazeni[1] > krajTrazeni[1]
                ) {
                    console.log(
                        "Pocetak termina mora biti prije kraja termina"
                    );
                    uslov = false;
                }
            }
            if (
                temp === periodicnaGlobal[i].semestar &&
                uslov &&
                sala === periodicnaGlobal[i].naziv
            ) {
                for (let j = 0; j < pon.length; j++) {
                    if (
                        pon[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 0
                    )
                        pon[j].className = "dani zauzeta 0";
                }
                for (let j = 0; j < uto.length; j++) {
                    if (
                        uto[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 1
                    )
                        uto[j].className = "dani zauzeta 1";
                }
                for (let j = 0; j < sri.length; j++) {
                    if (
                        sri[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 2
                    )
                        sri[j].className = "dani zauzeta 2";
                }
                for (let j = 0; j < cet.length; j++) {
                    if (
                        cet[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 3
                    )
                        cet[j].className = "dani zauzeta 3";
                }
                for (let j = 0; j < pet.length; j++) {
                    if (
                        pet[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 4
                    )
                        pet[j].className = "dani zauzeta 4";
                }
                for (let j = 0; j < sub.length; j++) {
                    if (
                        sub[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 5
                    )
                        sub[j].className = "dani zauzeta 5";
                }
                for (let j = 0; j < ned.length; j++) {
                    if (
                        ned[j].children[0].textContent != "" &&
                        periodicnaGlobal[i].dan === 6
                    )
                        ned[j].className = "dani zauzeta 6";
                }
            }
        }

        //Implementacija bojenja vanrednih sala
        /*
        {
            datum: '15.10.2019',
            pocetak: '13:00',
            kraj: '15:00',
            naziv: '0-01',
            predavac: 'Predavac'
        }
*/

        for (let i = 0; i < vanrednaGlobal.length; i++) {
            let datumVanredne = vanrednaGlobal[i].datum.split(".");
            datumVanredne[0] = Number(datumVanredne[0]);
            datumVanredne[1] = Number(datumVanredne[1]);
            datumVanredne[2] = Number(datumVanredne[2]);

            let pocetakSala = vanrednaGlobal[i].pocetak.split(":");
            let krajSala = vanrednaGlobal[i].kraj.split(":");

            for (let j = 0; j < pocetakTrazeni.length; j++)
                pocetakTrazeni[j] = Number(pocetakTrazeni[j]);

            for (let j = 0; j < krajTrazeni.length; j++)
                krajTrazeni[j] = Number(krajTrazeni[j]);

            for (let j = 0; j < pocetakSala.length; j++)
                pocetakSala[j] = Number(pocetakSala[j]);

            for (let j = 0; j < krajSala.length; j++)
                krajSala[j] = Number(krajSala[j]);

            let uslov = true;

            if (
                pocetakTrazeni[0] >= krajSala[0] ||
                krajTrazeni[0] <= pocetakSala[0]
            ) {
                if (
                    krajTrazeni[0] === pocetakSala[0] &&
                    krajTrazeni[1] > pocetakSala[1]
                )
                    uslov = true;
                else if (
                    pocetakTrazeni[0] === krajSala[0] &&
                    pocetakTrazeni[1] < krajSala[1]
                )
                    uslov = true;
                else uslov = false;
            }
            if (pocetakTrazeni[0] >= krajTrazeni[0]) {
                if (
                    pocetakTrazeni[0] === krajTrazeni[0] &&
                    pocetakTrazeni[1] > krajTrazeni[1]
                ) {
                    uslov = false;
                }
            }
            if (
                datumVanredne[1] === mjesec + 1 &&
                uslov &&
                sala === vanrednaGlobal[i].naziv
            ) {
                let dani = document.getElementsByClassName("dani");
                for (let j = 0; j < dani.length; j++) {
                    if (
                        Number(dani[j].children[0].textContent) ===
                        datumVanredne[0]
                    )
                        dani[j].className += " zauzeta";
                }
            }
        }
    }

    function ucitajPodatkeImpl(periodicna, vanredna) {
        //implementacija ide ovdje
        periodicnaGlobal = periodicna;
        vanrednaGlobal = vanredna;
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        //implementacija ide ovdje
        let dani = kalendarRef.getElementsByClassName("dani");

        for (let i = 0; i < dani.length; i++) {
            dani[i].className = "dani slobodna";
        }

        if (mjesec === "+1") {
            trenutniMjesec += 1;
        } else if (mjesec === "-1") {
            trenutniMjesec -= 1;
        } else {
            trenutniMjesec = mjesec;
        }
        mjesec = trenutniMjesec;

        if (trenutniMjesec === 0) {
            document.getElementById("prethodni").disabled = true;
        } else if (trenutniMjesec === 11) {
            document.getElementById("sljedeci").disabled = true;
        } else {
            document.getElementById("prethodni").disabled = false;
            document.getElementById("sljedeci").disabled = false;
        }

        var mjeseci = [
            "Januar",
            "Februar",
            "Mart",
            "April",
            "Maj",
            "Juni",
            "Juli",
            "August",
            "Septembar",
            "Oktobar",
            "Novembar",
            "Decembar"
        ];
        kalendarRef.children[0].innerHTML = mjeseci[mjesec];

        let d = new Date(new Date().getFullYear(), mjesec + 1, 0);
        let brojDanaUMjesecu = d.getDate();

        d = new Date(new Date().getFullYear(), mjesec, 1);

        let d_temp = d.getDay();
        if (d_temp === 0) {
            d_temp = 7;
        }
        let temp = 0;
        for (let i = 0; i < 38; i++) {
            if (i < d_temp - 1) {
                dani[i].children[0].innerHTML = "";
                dani[i].className = "dani";
            } else if (i >= d_temp - 1 && i < brojDanaUMjesecu + d_temp - 1) {
                dani[i].className += " slobodna";
                dani[i].children[0].innerHTML = i - d_temp + 2;
            } else {
                dani[i].className = "dani";
                dani[i].children[0].innerHTML = "";
            }
            dani[i].className += " " + temp.toString();
            temp++;
            if (temp === 7) temp = 0;
        }

        let e = document.getElementById("sale");
        obojiZauzecaImpl(
            kalendarRef,
            mjesec,
            e.options[e.selectedIndex].value,
            document.getElementById("pocetak").value,
            document.getElementById("kraj").value
        );
    }

    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    };
})();
//primjer korištenja modula
//Kalendar.obojiZauzeca(document.getElementById(“kalendar”),1,”1-15”,”12:00”,”13:30”);
//
