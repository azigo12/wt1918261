let assert = chai.assert;

describe("Crtanje kalendara", function() {
    describe("Testiranje metoda obojiZauzeca i ucitajPodatke", function() {
        it("Pozivanje obojiZauzeca kada podaci nisu učitani: očekivana vrijednost da se ne oboji niti jedan dan", function() {
            Kalendar.ucitajPodatke([], []);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeta = document.getElementsByClassName("zauzeta");
            //Dani ce se obojiti kada se u njihovu klasu doda i className zauzeta
            //document.getElementsByClassName("zauzeta"); nam daje niz svih elemenata koje imaju klasu zauzeta, to su ujedno i
            //elementi koji su obojeni, pa u ovom slucaju velicina ovog niza treba biti = 0.

            assert.equal(zauzeta.length, 0, "Ne treba da oboji nista");
        });

        it("Pozivanje obojiZauzeca gdje u zauzećima postoje duple vrijednosti za zauzeće istog termina,očekivano je da se dan oboji bez obzira što postoje duple vrijednosti", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "15.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);

            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "13:00",
                "15:00"
            );
            let dani = document.getElementsByClassName("zauzeta");

            //Treba samo jedan dan da bude obojen

            let brojac = 0;

            for (let i = 0; i < dani.length; i++) {
                if (dani[i].className.search("zauzeta") != -1) {
                    brojac++;
                }
            }
            assert.equal(brojac, 1, "Treba se obojiti jedan dan");
        });

        it("Pozivanje obojiZauzece kada u podacima postoji periodiČno zauzeće za drugi semestar: ocekivano je da se ne oboji zauzeće", function() {
            let periodicna = [
                {
                    dan: 0,
                    semestar: "ljetni",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeto = document.getElementsByClassName("zauzeto");

            //zauzeto nam predstavlja niz svih dana koji imaju klasu zauzeto, tj. koji su obojeni.
            // Treba da mu velicina bude 0

            assert.equal(zauzeto.length, 0, "Ne treba da oboji nista");
        });

        it("Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.12.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeto = document.getElementsByClassName("zauzeta");

            //Termin je rezervisan 15. decembra, a mi generisemo kalendar za novembar

            assert.equal(zauzeto.length, 0, "Ne treba da oboji nista");
        });

        //Testirano za mjesec novembar
        it("Pozivanje obojiZauzece kada su u podacima svi termini u mjesecu zauzeti: očekivano je da se svi dani oboje", function() {
            let periodicna = [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 1,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 2,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 3,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 4,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 5,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    dan: 6,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            let vanredna = [];

            //Posto sam unio periodicne rezervacije za dane 0-6, to ce znaciti da ce svi mjeseci u zimskom semestru biti rezervisani za termi
            //od 10:00 do 12:00.
            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeto = document.getElementsByClassName("zauzeta");

            //Znamo da novembar ima 30 dana pa provjeravamo da li je duzina niza zauzetih sala jednaka 30

            assert.equal(zauzeto.length, 30, "Svi dani obojeni");
        });

        it("Dva puta uzastopno pozivanje obojiZauzece: očekivano je da boja zauzeća ostane ista", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeca1 = document.getElementsByClassName("zauzeta");

            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeca2 = document.getElementsByClassName("zauzeta");
            //2 puta uzastopno pozivanje obojiZauzeca, zauzeca1 i zauzeca2 moraju biti ista

            assert.equal(
                zauzeca1,
                zauzeca2,
                "Zauzeca prije i poslije drugog, istog poziva metode obojiZauzeca moraju biti ista"
            );
        });

        it("Pozivanje ucitajPodatke, obojiZauzeca, ucitajPodatke - drugi podaci, obojiZauzeca: očekivano da se zauzeća iz prvih podataka ne ostanu obojena, tj. primjenjuju se samo posljednje učitani podaci", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            vanredna = [
                {
                    datum: "17.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );
            let zauzeto = document.getElementsByClassName("zauzeta");

            //datum je prvo postavljen na 15. pa prepravljen na 17., takode niz zauzetih sala treba da ima velicinu 1.

            let uslov = false;

            if (
                zauzeto.length === 1 &&
                zauzeto[0].children[0].textContent === "17"
            )
                uslov = true;

            assert.equal(uslov, true, "Ne treba da oboji nista");
        });

        it("Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali prije i poslije trazenog termina, te 3 termina koji se preklapaju na razlicite nacine: Ocekivano da se 3 termina oboje", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.11.2019",
                    pocetak: "8:00",
                    kraj: "10:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "16.11.2019",
                    pocetak: "9:00",
                    kraj: "11:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "17.11.2019",
                    pocetak: "10:00",
                    kraj: "11:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "18.11.2019",
                    pocetak: "11:00",
                    kraj: "13:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "19.11.2019",
                    pocetak: "12:00",
                    kraj: "14:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeto = document.getElementsByClassName("zauzeta");

            assert.equal(zauzeto.length, 3, "Treba da oboji 3 termina");
        });

        it("Pozivanje obojiZauzece kada u podacima postoje 2 zauzeća termina ali jedan u sali 0-01 a drugi u 1-01(Pretrazivati po 0-01)", function() {
            let periodicna = [];

            let vanredna = [
                {
                    datum: "15.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Predavac"
                },
                {
                    datum: "16.11.2019",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "1-01",
                    predavac: "Predavac"
                }
            ];

            Kalendar.ucitajPodatke(periodicna, vanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.obojiZauzeca(
                document.getElementById("kalendar"),
                10,
                "0-01",
                "10:00",
                "12:00"
            );

            let zauzeto = document.getElementsByClassName("zauzeta");

            assert.equal(
                zauzeto.length,
                1,
                "Treba samo jedan od dva dana da se oboji"
            );
        });
    });

    //
    //
    // Ovde pocinje testiranje za crtanje kalendara
    //
    describe("IscrtajKalendar(kalendarRef, mjesec)", function() {
        // Poziv za novembar
        it("Pozivanje iscrtajKalendar za mjesec sa 30 dana: očekivano je da se prikaže 30 dana", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);

            let brojac = 0;

            let dani = document.getElementsByClassName("dani");

            for (let i = 0; i < dani.length; i++) {
                if (dani[i].children[0].textContent != "") {
                    brojac++;
                }
            }
            assert.equal(brojac, 30, "Broj dana treba biti 30");
        });
        // Poziv za decembar
        it("iscrtajKalendar za mjesec sa 31 dan: očekivano je da se prikaže 31 dan", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);

            let brojac = 0;

            let dani = document.getElementsByClassName("dani");

            for (let i = 0; i < dani.length; i++) {
                if (dani[i].children[0].textContent != "") {
                    brojac++;
                }
            }
            assert.equal(brojac, 31, "Broj dana treba biti 31");
        });
        // Poziv za novembar
        it("iscrtajKalendar za trenutni mjesec: očekivano je da je 1. dan u petak", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);

            let dani = document.getElementsByClassName("dani");

            // Ukoliko je u 4. koloni u gridu onda je na poziciji petka.
            let jesteCetvrti = false;
            for (let i = 0; i < dani.length; i++) {
                if (dani[i].children[0].textContent === "1") {
                    if (i % 7 === 4) jesteCetvrti = true;
                }
            }

            assert.equal(
                jesteCetvrti,
                true,
                "1. u mjesecu novembar treba biti petak"
            );
        });
        // Poziv za novembar
        it("Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 30. dan u subotu", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);

            let dani = document.getElementsByClassName("dani");

            // Ukoliko je u 5. koloni u gridu onda je na poziciji subote.
            let jestePeti = false;
            for (let i = 0; i < dani.length; i++) {
                if (dani[i].children[0].textContent === "30") {
                    if (i % 7 === 5) jestePeti = true;
                }
            }

            assert.equal(
                jestePeti,
                true,
                "30. u mjesecu novembar treba biti subota"
            );
        });

        // Poziv za januar
        it("Pozivanje iscrtajKalendar za januar: očekivano je da brojevi dana idu od 1 do 31 počevši od utorka", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);

            let dani = document.getElementsByClassName("dani");

            //Provjera da li je 1. u utorak
            let jeste1 = false;
            for (let i = 0; i < dani.length; i++) {
                if (dani[i].children[0].textContent === "1") {
                    if (i % 7 === 1) jeste1 = true;
                }
            }

            //Provjera da li se ispisuju dani redom od 1 do 31
            let daniRedom = 1;
            let iduRedom = true;
            for (let i = 0; i < dani.length; i++) {
                if (
                    dani[i].children[0].textContent != "" &&
                    daniRedom != Number(dani[i].children[0].textContent)
                ) {
                    iduRedom = false;
                } else if (dani[i].children[0].textContent === "") {
                } else daniRedom++;
            }
            let broj = 0;
            if (iduRedom && jeste1) broj = 1;

            assert.equal(
                broj,
                1,
                "1. treba biti u utorak i dani idu redom od 1 do 31"
            );
        });

        // Kada se nacrta kalendar za januar, da li je dugme prethodni blokirano
        it("Pozivanje iscrtajKalendar za januar: očekivano je da dugme pethoni bude blokirano", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);

            let prethodni = document.getElementById("prethodni");

            let jesteBlokiran = false;

            if (prethodni.disabled === true) jesteBlokiran = true;
            assert.equal(
                jesteBlokiran,
                true,
                "Dugme prethodni treba da bude blokirano"
            );
        });

        // Kada se nacrta kalendar za decembar, da li je dugme sljedeci blokirano
        it("Pozivanje iscrtajKalendar za decembar: očekivano je da dugme sljedeci bude blokirano", function() {
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);

            let sljedeci = document.getElementById("sljedeci");

            let jesteBlokiran = false;

            if (sljedeci.disabled === true) jesteBlokiran = true;
            assert.equal(
                jesteBlokiran,
                true,
                "Dugme sljedeci treba da bude blokirano"
            );
        });
    });
});
