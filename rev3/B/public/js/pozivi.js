const Pozivi = (function () {
    function ucitajPodatke() {
        var ajax = new XMLHttpRequest();
        ajax.open("GET", "http://localhost:8080/ucitajPodatke", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
        ajax.onreadystatechange = function () {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                odg = JSON.parse(ajax.response);
                Kalendar.ucitajPodatke(odg.periodicna, odg.vanredna);
                Kalendar.obojiZauzeca(
                    "vanjska",
                    Mjesec.trenutniMjesec(),
                    document.getElementById("odabranaSala").value,
                    document.getElementById("pocetak").value,
                    document.getElementById("kraj").value);
            }
        }
    }
    function rezervisiTermin(sala, d, periodicna, pocetak, kraj) {
        let semestar, obj;
        if (d.getMonth() >= 9 || d.getMonth() == 0) {
            semestar = "zimski";
        } else if (d.getMonth() >= 2 && d.getMonth() <= 5) {
            semestar = "ljetni";
        }
        if (periodicna) {
            obj = {
                dan: (d.getDay() == 0) ? 6 : d.getDay() - 1, //ako je nedjelja postavi na 6, inace smanji za 1
                semestar: semestar,
                pocetak: pocetak,
                kraj: kraj,
                naziv: sala,
                predavac: "Nedim Ramic"
            }
        } else {
            obj = {
                datum: "" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                pocetak: pocetak,
                kraj: kraj,
                naziv: sala,
                predavac: "Sedad Kamencic"
            }
        }
        var ajax = new XMLHttpRequest();
        ajax.open("POST", "http://localhost:8080/rezervacija", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(obj));
        ajax.onreadystatechange = function () {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                //odg = JSON.parse(ajax.response);
                try {
                    odg = JSON.parse(ajax.response);
                } catch{
                    alert(ajax.response);
                    ucitajPodatke();
                }
                Kalendar.ucitajPodatke(odg.periodicna, odg.vanredna);
                Kalendar.obojiZauzeca(
                    "vanjska",
                    Mjesec.trenutniMjesec(),
                    document.getElementById("odabranaSala").value,
                    document.getElementById("pocetak").value,
                    document.getElementById("kraj").value);
            }
        }
    }
    function ucitajTriSlike(indeksPrveSlike) {
        let ajax = new XMLHttpRequest();
        url = "http://localhost:8080/ucitajTriSlike?indeks=" + indeksPrveSlike;
        ajax.open("GET", url, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                odg = JSON.parse(ajax.response);
                return odg;
            }
        }
    }

    return {
        ucitajPodatke: ucitajPodatke,
        rezervisiTermin: rezervisiTermin,
        ucitajTriSlike: ucitajTriSlike
    }
}());