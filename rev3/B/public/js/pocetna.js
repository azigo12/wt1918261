function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
 }

var i = 0;
//funkcija koja odlucuje da li ucitati slike iz local storage ili pokupiti sa servera
async function ucitajSlike(indeks) {
    i = indeks;
    let prva = document.getElementById("prva");
    let druga = document.getElementById("druga");
    let treca = document.getElementById("treca");
    if (window.localStorage.getItem(i)) {
        //slike su vec prije ucitane, samo ih treba prikazati
        prva.src = window.localStorage.getItem(i);
        druga.src = window.localStorage.getItem(i + 1);
        treca.src = window.localStorage.getItem(i + 2);
    } else {
        odg = Pozivi.ucitajTriSlike(i);
        await sleep(100); //da dadnemo dovoljno vremena ajax pozivu da vrati podatke o slikama
        //moglo bi se uraditi i preko callback funkcije i  jquery-a, ali je 4:15AM trenutno, i nemam dovoljno kafe pored sebe
        //za takav poduhvat.
        let l = Object.keys(odg);
        l.forEach(element => {
            window.localStorage.setItem(element, odg[element]);
        });
    }

    prva.src = window.localStorage.getItem(i) ? window.localStorage.getItem(i) : " ";
    druga.src = window.localStorage.getItem(i + 1) ? window.localStorage.getItem(i + 1) : " ";
    treca.src = window.localStorage.getItem(i + 2) ? window.localStorage.getItem(i + 2) : " ";
}

function dugmePrethodni() {
    //smanjimo i za 3 zatim pozovemo ucitajSlike(i)
    document.getElementById("sljedeci").disabled = false;
    if (i >= 3) {
        i -= 3;
    } else {
        i = 0;
        document.getElementById("prethodni").disabled = true;
    }
    ucitajSlike(i);
}

function dugmeSljedeci() {
    //prvo odredimo indeks najvece slike
    document.getElementById("prethodni").disabled = false;
    let max = localStorage.getItem("brojSlika");
    if (i <= max - 3) {
        i += 3;
    } else {
        i = max;
        document.getElementById("sljedeci").disabled = true;
    }
    ucitajSlike(i);
}

window.onload = function () {
    Pozivi.ucitajTriSlike(i);
    ucitajSlike(i);
}
if (i == 0) {
    document.getElementById("prethodni").disabled = true;
}