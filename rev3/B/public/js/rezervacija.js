window.onload = Pozivi.ucitajPodatke();

function test(ref){
    let odg = confirm("kliknuta celija");
    let datum = ref.getElementsByClassName("datum");
    odg? console.log(datum[0]) : console.log("ne dam ti datum");
}

function rezervacijaTermina(ref){
    let odg = confirm("Da li želite rezervisati ovaj termin?");
    if(odg){
        let sala = document.getElementById("odabranaSala").value;
        let periodicna = document.getElementById("period").checked;
        let pocetak = document.getElementById("pocetak").value;
        let kraj = document.getElementById("kraj").value;
        let mjesec =Mjesec.mjesecBroj(document.getElementById("mjesec").innerHTML.toLowerCase());
        if(mjesec>5 && mjesec<9){
            alert("Raspuust, pustite djecu na miru")
            return;
        }
        if(ref.getElementsByClassName("slobodna").length == 0){
            alert("Odabrana sala je vec zauzeta");
            return;
        }
        let datum = ref.getElementsByClassName("datum")[0].innerHTML;
        let d = new Date(2019, mjesec, datum);
        if(pocetak=="" || kraj==""){
            alert("Unesite vrijednosti za pocetak i kraj");
            return;
        } else{
           Pozivi.rezervisiTermin(sala, d, periodicna, pocetak, kraj)
        }
    }
}