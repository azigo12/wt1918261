
var podaci = [];
let Mjesec = (function () {
    let _mjesecBroj = {
        'januar': 0,
        'februar': 1,
        'mart': 2,
        'april': 3,
        'maj': 4,
        'juni': 5,
        'juli': 6,
        'august': 7,
        'septembar': 8,
        'oktobar': 9,
        'novembar': 10,
        'decembar': 11
    }
    let _mjesecString = {
        0: 'januar',
        1: 'februar',
        2: 'mart',
        3: 'april',
        4: 'maj',
        5: 'juni',
        6: 'juli',
        7: 'august',
        8: 'septembar',
        9: 'oktobar',
        10: 'novembar',
        11: 'decembar',
    }
    let _trenutniMjesec = new Date().getMonth();
    function trenutniMjesec(mjesec = -1) {
        if (mjesec != -1) {
            _trenutniMjesec = mjesec;
        }
        return _trenutniMjesec
    }
    function mjesecString(m) {
        return _mjesecString[m];
    }
    function mjesecBroj(b) {
        return _mjesecBroj[b];
    }
    function brojDana(month = -1) {
        if (month == -1) {
            month = _trenutniMjesec;
        }
        return new Date(2019, month + 1, 0).getDate();
    }
    function sljedeci() {
        (_trenutniMjesec < 11) ? _trenutniMjesec += 1 : null;
        return _trenutniMjesec;

    }
    function prethodni() {
        (_trenutniMjesec > 0) ? _trenutniMjesec -= 1 : null;
        return _trenutniMjesec;
    }
    function daniMjeseca(month, day_ba) {
        day = {
            0: 1,
            1: 2,
            2: 3,
            3: 4,
            4: 5,
            5: 6,
            6: 0
        }
        d = new Date(2019, Mjesec.mjesecBroj(month), 1);
        month = d.getMonth();
        days = [];

        d.setDate(1);

        // Get the first wanted day in the month
        while (d.getDay() !== day[day_ba]) {
            d.setDate(d.getDate() + 1);
        }

        // Get all the other wanted days in the month
        while (d.getMonth() === month) {
            days.push(new Date(d.getTime()));
            d.setDate(d.getDate() + 7);
        }

        return days;
    }
    let day_en_ba = {
        0: 6,
        1: 0,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 5
    }
    function trenutniPrviDan() {
        return day_en_ba[new Date(2019, _trenutniMjesec, 1).getDay()];
    }
    function trenutniZadnjiDan() {
        return day_en_ba[new Date(2019, _trenutniMjesec, 30).getDay()];
    }
    return {
        trenutniMjesec: trenutniMjesec,
        mjesecString: mjesecString,
        mjesecBroj: mjesecBroj,
        brojDana: brojDana,
        sljedeci: sljedeci,
        prethodni: prethodni,
        daniMjeseca: daniMjeseca,
        trenutniPrviDan: trenutniPrviDan,
        trenutniZadnjiDan: trenutniZadnjiDan,
    }
}());
function dajTermine(zauzece) {
    //vraca niz zauzeca, gdje je element tipa Zauzece
    let termini = [];
    if (zauzece["semestar"].toUpperCase() == "ZIMSKI") {
        //termini su u mjesecima oktobar, novembar, decembar, januar
        let dani = Mjesec.daniMjeseca('oktobar', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('novembar', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('decembar', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('januar', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
    } else {
        let dani = Mjesec.daniMjeseca('februar', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('mart', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('april', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('maj', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
        dani = Mjesec.daniMjeseca('juni', zauzece.dan)
        for (dan in dani) {
            termini.push({
                datum: dani[dan],
                pocetak: zauzece.pocetak,
                kraj: zauzece.kraj,
                naziv: zauzece.naziv,
                predavac: zauzece.predavac
            });
        }
    }
    return termini;
}

let Kalendar = (function () {

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, vrijemePocetka, vrijemeKraja) {
        let element = document.getElementById(kalendarRef);
        // if (element == null || element == undefined) {
        //     alert("greska, nepostojeci element1");
        //     return;
        // }
        // //provjera da li je poslan mjesec u rangu (0,11)
        // if (mjesec < 1 || mjesec > 11) {
        //     alert("greska, nepostojeci mjesec2");
        //     return;
        // }
        // //regex za provjeru unosa vremena, pretpostavka je da se unosi vrijeme u 24h formatu
        // let reg = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$"
        // if (!reg.test(vrijemePocetka)) {
        //     alert("Greska, vrijeme pocetka u nedozvoljenom formatu, potreban format hh:mm");
        //     return;
        // }
        // if (!reg.test(vrijemeKraja)) {
        //     alert("Greska, vrijeme kraja u nedozvoljenom formatu, potreban format hh:mm");
        //     return;
        // }
        if (mjesec == Mjesec.trenutniMjesec()) {
            //pokupi one termine iz podaci koji su za taj mjesec i tu salu
            var odgovarajuciTermini = [];
            podaci.forEach(element => {
                if (element.datum.getMonth() == mjesec &&
                    element.naziv == sala &&
                    ((element.pocetak >= vrijemePocetka && element.pocetak <= vrijemeKraja) ||
                    (element.kraj >= vrijemePocetka && element.kraj <= vrijemeKraja))) {
                    odgovarajuciTermini.push(element);
                }
            });
            //sad kada imamo odgovarajuce termine, obojimo datume
            //prodjemo kroz svaku sedmicu, uzmemo datum, ako se taj datum nalazi
            //medju odgovarajucim terminima, promijenicemo mu klasu u zauzeta
            celije = document.getElementsByClassName("datum");
            for (let i = 0; i < celije.length; i++) {
                for (let j = 0; j < odgovarajuciTermini.length; j++) {
                    if (celije.item(i).innerHTML == odgovarajuciTermini[j].datum.getDate()) {
                        var odgCelija = celije.item(i).parentNode.parentNode.parentNode;
                        odgCelija.getElementsByClassName("slobodna")[0] ? odgCelija.getElementsByClassName("slobodna")[0].className = "zauzeta" : null;
                    }
                }
            }
        }
    }
    //periodicna - niz periodicnih zauzeca, vanredna - niz vanredih zauzeca
    function ucitajPodatkeImpl(periodicna, vanredna) {
        podaci = [];
        periodicna.forEach(element => {
            podaci = podaci.concat(dajTermine(element));
        });
        //Tu ce biti svi podaci o tome koje sale i u kojim terminima treba obojiti.
        // sala, datum, vrijemePocetka, vrijemeKraja, profesor
        //ovdje se treba pozvati obojiZauzeca
        vanredna.forEach(element => {
            element.datum = new Date(element.datum);
            podaci = podaci.concat(element);
        })
        return podaci;
    }
    function generisiCeliju(broj, status) {
        return `
            <td>
                <table class="unutrasnja" onclick="rezervacijaTermina(this)">
                    <tr>
                        <td class="datum">${broj}</td>
                    </tr>
                    <tr>
                        <td class="${status}"></td>
                    </tr>
                </table>
            </td>`;
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        if (mjesec < 0 || mjesec > 11) {
            return;
        }
        //odrediti prvi dan u mjesecu, odrediti broj dana
        //na osnovu prvog dana i broja dana iscrtati kalendar
        Mjesec.trenutniMjesec(mjesec);
        let praznaCelija = '<td class="emptyCell"></td>';
        let d = new Date();
        d.setDate(1);
        d.setMonth(Mjesec.trenutniMjesec());
        d.setFullYear(2019);
        let prazneCelije = 0;
        if (d.getDay() == 0) {
            prazneCelije = 6;
        } else {
            prazneCelije = d.getDay() - 1;
        }
        let brojDana = Mjesec.brojDana(Mjesec.trenutniMjesec());
        let preostalo = brojDana;
        let mjesecTekst = Mjesec.mjesecString(Mjesec.trenutniMjesec());
        document.getElementById("mjesec").innerHTML = mjesecTekst.charAt(0).toUpperCase() + mjesecTekst.slice(1);
        document.getElementById("prva-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            if (i < prazneCelije) {
                document.getElementById("prva-sedmica").innerHTML += praznaCelija;
            } else {
                document.getElementById("prva-sedmica").innerHTML += generisiCeliju(i - prazneCelije + 1, "slobodna");
                preostalo--;
            }
        }
        document.getElementById("druga-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            document.getElementById("druga-sedmica").innerHTML += generisiCeliju(brojDana - preostalo + 1, "slobodna");
            preostalo--;
        }
        document.getElementById("treca-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            document.getElementById("treca-sedmica").innerHTML += generisiCeliju(brojDana - preostalo + 1, "slobodna");
            preostalo--;
        }
        document.getElementById("cetvrta-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            document.getElementById("cetvrta-sedmica").innerHTML += generisiCeliju(brojDana - preostalo + 1, "slobodna");
            preostalo--;
        }
        document.getElementById("peta-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            if (preostalo > 0) {
                document.getElementById("peta-sedmica").innerHTML += generisiCeliju(brojDana - preostalo + 1, "slobodna");
                preostalo--;
            } else {
                document.getElementById("peta-sedmica").innerHTML += praznaCelija;
            }
        }
        document.getElementById("sesta-sedmica").innerHTML = "";
        for (let i = 0; i < 7; i++) {
            if (preostalo > 0) {
                document.getElementById("sesta-sedmica").innerHTML += generisiCeliju(brojDana - preostalo + 1, "slobodna");
                preostalo--;
            }
            else {
                document.getElementById("sesta-sedmica").innerHTML += praznaCelija;
            }
        }
        Kalendar.obojiZauzeca(
            "vanjska",
            Mjesec.trenutniMjesec(),
            document.getElementById("odabranaSala").value,
            document.getElementById("pocetak").value,
            document.getElementById("kraj").value);
    }


    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl,
    }
}());

function crtaj() {
    Kalendar.iscrtajKalendar(document.getElementById("vanjska"), Mjesec.trenutniMjesec());
}
function dugmeSljedeci() {
    Kalendar.iscrtajKalendar(document.getElementById("vanjska"), Mjesec.sljedeci());
}
function dugmePrethodni() {
    Kalendar.iscrtajKalendar(document.getElementById("vanjska"), Mjesec.prethodni());
}
Kalendar.iscrtajKalendar(document.getElementById("vanjska"), Mjesec.trenutniMjesec());