const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

app.get(['/', '/index', '/pocetna'], function (req, res) {
    res.sendFile(__dirname + '/public/views/pocetna.html');
});

app.get('/sale', function (req, res) {
    res.sendFile(__dirname + '/public/views/sale.html');
});

app.get('/rezervacija', function (req, res) {
    res.sendFile(__dirname + '/public/views/rezervacija.html');
});

app.get('/unos', function (req, res) {
    res.sendFile(__dirname + '/public/views/unos.html');
});

app.get('/ucitajPodatke', function (req, res) {
    res.sendFile(__dirname + '/public/js/zauzeca.json');
});
danString = {
    0: 'ponedjeljak',
    1: 'utorak',
    2: 'srijeda',
    3: 'cetvrtak',
    4: 'petak',
    6: 'subota',
    7: 'nedjelja',
}
app.post('/rezervacija', function (req, res) {
    let zauzece = req.body;
    let data = fs.readFileSync(__dirname + '/public/js/zauzeca.json');
    let file = JSON.parse(data);
    if (zauzece.dan) {
        //ako zauzece ima atribut dan, znaci da je periodicno
        //provjerimo postoji li poklapanje prije dodavanja
        file.periodicna.forEach(element => {
            if (element.naziv == zauzece.naziv &&
                element.semestar == zauzece.semestar &&
                ((element.pocetak >= zauzece.pocetak && element.pocetak <= zauzece.kraj) ||
                    (element.kraj >= zauzece.pocetak && element.kraj <= zauzece.kraj))) {
                res.send(`Nije moguće rezervisati salu ${zauzece.naziv} za navedeni dan ${danString[zauzece.dan]} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`)
            }
        });
        file.periodicna.push(zauzece);
    } else {
        file.vanredna.forEach(element => {
            if (element.naziv == zauzece.naziv &&
                element.datum == zauzece.datum &&
                ((element.pocetak >= zauzece.pocetak && element.pocetak <= zauzece.kraj) ||
                    (element.kraj >= zauzece.pocetak && element.kraj <= zauzece.kraj))) {
                res.send(`Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${zauzece.datum} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`)
            }
        });
        file.vanredna.push(zauzece);
    }
    fs.writeFileSync(__dirname + '/public/js/zauzeca.json', JSON.stringify(file));
    res.sendFile(__dirname + '/public/js/zauzeca.json');
});
slike = {
    0: 'https://i.picsum.photos/id/570/400/400.jpg',
    1: 'https://i.picsum.photos/id/571/400/400.jpg',
    2: 'https://i.picsum.photos/id/572/400/400.jpg',
    3: 'https://i.picsum.photos/id/573/400/400.jpg',
    4: 'https://i.picsum.photos/id/574/400/400.jpg',
    5: 'https://i.picsum.photos/id/575/400/400.jpg',
    6: 'https://i.picsum.photos/id/576/400/400.jpg',
    7: 'https://i.picsum.photos/id/577/400/400.jpg',
    8: 'https://i.picsum.photos/id/579/400/400.jpg',
    9: 'https://i.picsum.photos/id/581/400/400.jpg',
    10: 'https://i.picsum.photos/id/370/400/400.jpg',
    11: 'https://i.picsum.photos/id/471/400/400.jpg',
    12: 'https://i.picsum.photos/id/472/400/400.jpg',
    13: 'https://i.picsum.photos/id/473/400/400.jpg',
    14: 'https://i.picsum.photos/id/474/400/400.jpg',
    14: 'https://i.picsum.photos/id/474/400/400.jpg',
    16: 'https://i.picsum.photos/id/476/400/400.jpg',
    17: 'https://i.picsum.photos/id/477/400/400.jpg',
    18: 'https://i.picsum.photos/id/479/400/400.jpg',
    19: 'https://i.picsum.photos/id/481/400/400.jpg',
}
var brojac =0;
app.get('/ucitajTriSlike', function (req, res) {
    brojac++;
    //jednostavan nacin provjere kad i koliko puta se pozove funkcija kada se testira 3. zadatak
    console.log("pozvana "+ brojac+" puta");
    p = req.query;
    prva = parseInt(p.indeks);
    druga = parseInt(p.indeks) + 1;
    treca = parseInt(p.indeks) + 2;
    obj = {}
    obj["brojSlika"] = Object.keys(slike).length;;
    obj[prva] = slike[prva]? slike[prva] : null;
    obj[druga] = slike[druga]? slike[druga] : null;
    obj[treca] = slike[treca]? slike[treca] : null;
    
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(obj));
});
app.listen(8080);