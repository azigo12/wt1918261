var htmlRef;
var  ucitavanjeSlikaAjax=(function(){
    var konstruktor=function(divSlika1, divSlika2, divSlika3) {
        var ajax=new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let odgovor=JSON.parse(ajax.responseText);
                
                for(var i=0;i<3;i++){
                    let sadrzaj=' ';
                    if(i==0){
                        sadrzaj=sadrzaj+'<img src="'+odgovor.slika1+'" alt="slikica" class="zaSlike">';
                        divSlika1.innerHTML=sadrzaj;
                    }
                    else if(i==1){
                        sadrzaj=sadrzaj+'<img src="'+odgovor.slika2+'" alt="slikica" class="zaSlike">';
                        divSlika2.innerHTML=sadrzaj;
                    }
                    else{
                        sadrzaj=sadrzaj+'<img src="'+odgovor.slika3+'" alt="slikica" class="zaSlike">';
                        divSlika3.innerHTML=sadrzaj;
                    }
                   
                }


            }
        };
        ajax.open('GET','http://localhost:8080/slike?brSlike='+1,true);
        ajax.setRequestHeader("Content-Type","application/json");
        ajax.send();
        return {
            vratiSlike:function(brojSlika) {
                ajax.open('GET','http://localhost:8080/slike?brSlike='+brojSlika,true);
                ajax.setRequestHeader("Content-Type","application/json");
                ajax.send();
            }
        }
    };

    return konstruktor;
}());
var div1=document.getElementById("slika1");
var div2=document.getElementById("slika2");
var div3=document.getElementById("slika3");
var a=new ucitavanjeSlikaAjax(div1,div2,div3);
var i=1;
var dugmelijevo=document.getElementById("prethodneSlike");
var dugmedesno=document.getElementById("slijedeceSlike");
function desnoSlika(){
    a.vratiSlike(i+1);
    i++;    
    if(i==4){ 
        div2.hidden=true;
        div3.hidden=true;
        dugmedesno.disabled = true;
        dugmelijevo.disabled=false;
    } else {
        div2.hidden=false;
        div3.hidden=false;
        dugmedesno.disabled = false;
        dugmelijevo.disabled=false;

    }
}
function lijevoSlika() {
    a.vratiSlike(i-1);
    i--;
    if(i==1) {
        dugmelijevo.disabled = true;
        dugmedesno.disabled=false;
    } else {
        div2.hidden=false;
        div3.hidden=false;
        dugmelijevo.disabled = false;
        dugmedesno.disabled=false;

    }
}

let Pozivi = (function () {
    // privatni atributi

    

    function ucitajImpl(zauzeca) {

            let xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    JSONzauzeca = JSON.parse(xhttp.responseText);

                    console.log(JSONzauzeca);
                    Kalendar.ucitajPodatke(JSONzauzeca.periodicna, JSONzauzeca.redovna);
                }
            };

            xhttp.open("GET", zauzeca , true);
            xhttp.send();
        }
    

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
    }

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
    }
   

    return {

        obojiZauzeca: obojiZauzecaImpl,
        ucitaj: ucitajImpl,
        iscrtajKalendar: iscrtajKalendarImpl,
    }
}());