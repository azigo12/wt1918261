const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var path = require('path');
const fs = require('fs');
const xhttp = require('http');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

//app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req,res){
     res.sendFile(__dirname  + '/public/html/pocetna.html');
       
});

app.get('/slike', function(req,res) {
    let brojSlike=req.query.brSlike;
    if(brojSlike==1) {
        res.json({slika1:"/photos/slika1.jpg",slika2:"/photos/slika2.jpg",
                    slika3:"/photos/slika3.jpg"});
    }
    if(brojSlike==2) {res.json({slika1:"/photos/slika4.jpg",slika2:"/photos/slika5.jpg",slika3:"/photos/slika6.jpg"});
                
    }
    if(brojSlike==3) {
        res.json({slika1:"/photos/slika7.jpg",slika2:"/photos/slika8.jpg",slika3:"/photos/slika9.jpg"});
    }
    if(brojSlike==4) {
        res.json({slika1:"/photos/slika10.jpg"});
       
    }
});

app.listen(8080);